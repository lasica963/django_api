from django.urls import path, include
from .views import Films, Film, Posts, SinglePost, FavFilm

urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('films/', Films.as_view()),
    path('films/<int:id>/', Film.as_view()),
    path('posts/', Posts.as_view()),
    path('posts/<int:id>/', SinglePost.as_view()),
    path('fav/', FavFilm.as_view()),
]
