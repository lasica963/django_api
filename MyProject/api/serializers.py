from rest_framework import serializers
from djoser.serializers import UserCreateSerializer
from .models import SingleFilm, User, Post, FavouritesFilm


class SingleFilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = SingleFilm
        fields = ['id', 'title', 'director', 'type', 'country', 'premierDate', 'trailer']


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'author', 'text', 'date']


class RegisterUserSerializer(UserCreateSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'first_name', 'last_name', 'phone']


class FavFilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = FavouritesFilm
        fields = ['id', 'film', 'user']


class GetFavouritesFilm(serializers.ModelSerializer):
    film = SingleFilmSerializer(read_only=True)
    class Meta:
        model = FavouritesFilm
        fields = ['film']



