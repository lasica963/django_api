from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime
# Create your models here.


class User(AbstractUser):
    email = models.EmailField(verbose_name='email', max_length=255, unique=True)
    password = models.CharField(null=True, max_length=20)
    phone = models.CharField(null=True, max_length=20)
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']
    USERNAME_FIELD = 'email'

    def get_username(self):
        return self.email


class SingleFilm(models.Model):
    title = models.CharField(max_length=100)
    director = models.CharField(max_length=35)
    type = models.CharField(max_length=20)
    country = models.CharField(max_length=50)
    premierDate = models.CharField(max_length=15, default='00-00-0000')
    trailer = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(max_length=125)
    author = models.CharField(max_length=125)
    text = models.TextField()
    date = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.title


class FavouritesFilm(models.Model):
    user = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE)
    film = models.ForeignKey(SingleFilm, related_name='+', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'film']

