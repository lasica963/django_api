# Generated by Django 3.0.6 on 2020-06-09 19:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_singlefilm_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='singlefilm',
            name='image',
        ),
    ]
