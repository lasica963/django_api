from django.contrib import admin
from .models import SingleFilm, User, Post
# Register your models here

admin.site.register(SingleFilm)
admin.site.register(User)
admin.site.register(Post)
