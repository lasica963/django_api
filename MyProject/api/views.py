from rest_framework import permissions
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from django.http import HttpResponse, JsonResponse
from .models import SingleFilm, Post, FavouritesFilm
from .serializers import SingleFilmSerializer, PostSerializer, FavFilmSerializer, GetFavouritesFilm

# helper permissions decorator
def permissions_decorator(classes):
    def decorator(func):
        def decorated_func(self, *args, **kwargs):
            self.permission_classes = classes
            # this call is needed for request permissions
            self.check_permissions(self.request)
            return func(self, *args, **kwargs)
        return decorated_func
    return decorator


# Get posts, add post
class Posts(APIView):

    def get(self, request):
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return JsonResponse(serializer.data, safe=False)

    @permissions_decorator((permissions.IsAuthenticated,))
    def post(self, request):
        data = JSONParser().parse(request)
        serializer = PostSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


# Get post, update post, delete post
class SinglePost(APIView):

    def get_object(self, pk):
        try:
            return Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise HttpResponse(status=404)

        serializer = PostSerializer(post)
        return JsonResponse(serializer.data)

    def get(self, request, id):

        post = self.get_object(id)
        serializer = PostSerializer(post)
        return JsonResponse(serializer.data)

    @permissions_decorator((permissions.IsAuthenticated,))
    def put(self, request, id):
        post = self.get_object(id)
        data = JSONParser().parse(request)
        serializer = PostSerializer(post, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    @permissions_decorator((permissions.IsAuthenticated,))
    def delete(self, request, id):
        post = self.get_object(id)

        post.delete()
        return HttpResponse(status=204)


# Get films, Add film
class Films(APIView):

    def get(self, request):
        films = SingleFilm.objects.all()
        serializer = SingleFilmSerializer(films, many=True)
        return JsonResponse(serializer.data, safe=False)

    @permissions_decorator((permissions.IsAuthenticated,))
    def post(self, request):
        data = JSONParser().parse(request)
        serializer = SingleFilmSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


# get single film, update film, delete film
class Film(APIView):

    def get_object(self, pk):
        try:
            return SingleFilm.objects.get(pk=pk)
        except SingleFilm.DoesNotExist:
            raise HttpResponse(status=404)

    def get(self, request, id):

        film = self.get_object(id)
        serializer = SingleFilmSerializer(film)
        return JsonResponse(serializer.data)

    @permissions_decorator((permissions.IsAuthenticated,))
    def put(self, request, id):
        film = self.get_object(id)

        data = JSONParser().parse(request)
        serializer = SingleFilmSerializer(film, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    @permissions_decorator((permissions.IsAuthenticated,))
    def delete(self, request, id):
        film = self.get_object(id)

        film.delete()
        return HttpResponse(status=204)


class FavFilm(APIView):

    @permissions_decorator((permissions.IsAuthenticated,))
    def get(self, request):
        films = FavouritesFilm.objects.filter(user_id=request.user.id)
        serializer = GetFavouritesFilm(films, many=True)
        return JsonResponse(serializer.data, safe=False)

    @permissions_decorator((permissions.IsAuthenticated,))
    def post(self, request):
        data = JSONParser().parse(request)
        serializer = FavFilmSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
